# matrix4cj 库

### 介绍

matrix4cj 是一个基本线性代数包，用于构造和操作真实密集矩阵的库。

### 1 构造和操作真实密集矩阵

需求来源：三方库
用户：仓颉开发者
场景：构造和操作真实密集矩阵
目标：实现构造和操作真实密集矩阵
验收标准：正确构造和操作真实密集矩阵

##### 1.1 主要接口

```cangjie
public class Matrix {
    /*
     * 构建一个 m 乘 n 的零点矩阵, m,n 非法值时抛 NegativeArraySizeException
     * 参数 m - 行数
     * 参数 n - 列数
     */
    public init(m: Int64, n: Int64)

    /*
     * 构造一个 m 乘 n 的常量矩阵，m,n 非法值时抛 NegativeArraySizeException
     * 参数 m - 行数
     * 参数 n - 列数
     * 参数 s - 用这个标量值填充矩阵
     */
    public init(m: Int64, n: Int64, s: Float64)

    /*
     * 用二维双数组构建矩阵。数组不符合矩阵格式抛 IllegalArgumentException
     * 参数 A - 二维 Float64 数组
     */
    public init(A: Array<Array<Float64>>)

    /*
     * 不检查参数快速构建矩阵
     * 参数 A - 二维 Float64 数组
     * 参数 m - 行数
     * 参数 n - 列数
     */
    public init(A: Array<Array<Float64>>, m: Int64, n: Int64)

    /*
     * 用一维数组构建矩阵，数组长度必须是 m 的倍数，否则抛 IllegalArgumentException
     * 参数 A - 一维 Float64 数组
     * 参数 m - 行数
     */
    public init(vals: Array<Float64>, m: Int64)

    /*
     * 从二维数组的副本中构建矩阵， 数组不符合矩阵格式抛 IllegalArgumentException
     * 参数 A - 二维 Float64 数组
     */
    public static func constructWithCopy(A: Array<Array<Float64>>): Matrix

    /*
     * 复制矩阵
     * 返回值 Matrix - 复制的新矩阵
     */
    public func copy(): Matrix

    /*
     * 复制矩阵
     * 返回值 Object - 复制的新矩阵对象
     */
    public func clone(): Object

    /*
     * 获取内部二维数组
     * 返回值 Array<Array<Float64>> - 内部二维数组
     */
    public func getArray(): Array<Array<Float64>>

    /*
     * 复制内部二维数组
     * 返回值 Array<Array<Float64>> - 内部二维数组
     */
    public func getArrayCopy(): Array<Array<Float64>>

    /*
     * 创建内部数组的一维列打包副本。
     * 返回值 Array<Float64> - 按列打包在一维数组中的矩阵元素
     */
    public func getColumnPackedCopy(): Array<Float64>

    /*
     * 创建内部数组的一维列打包副本。
     * 返回值 Array<Float64> - 按行打包在一维数组中的矩阵元素
     */
    public func getRowPackedCopy(): Array<Float64>

    /*
     * 获取矩阵行数
     * 返回值 Int64 - 矩阵行数
     */
    public func getRowDimension(): Int64

    /*
     * 获取矩阵列数
     * 返回值 Int64 - 矩阵列数
     */
    public func getColumnDimension(): Int64

    /*
     * 获取单个元素，元素不存在抛 IndexOutOfBoundsException
     * 参数 i - 行索引
     * 参数 j - 列索引
     * 返回值 Float64 - 元素值
     */
    public func get(i: Int64, j: Int64): Float64

    /*
     * 获取子矩阵
     * 参数 i0 - 行起始索引
     * 参数 i1 - 行结束索引
     * 参数 j0 - 列起始索引
     * 参数 j1 - 列结束索引
     * 返回值 Matrix - 子矩阵
     */
    public func getMatrix(i0: Int64, i1: Int64, j0: Int64, j1: Int64): Matrix

    /*
     * 获取子矩阵
     * 参数 r - 行索引数组
     * 参数 c - 列索引数组
     * 返回值 Matrix - 子矩阵
     */
    public func getMatrix(r: Array<Int64>, c: Array<Int64>): Matrix

    /*
     * 获取子矩阵
     * 参数 i0 - 行起始索引
     * 参数 i1 - 行结束索引
     * 参数 c - 列索引数组
     * 返回值 Matrix - 子矩阵
     */
    public func getMatrix(i0: Int64, i1: Int64, c: Array<Int64>): Matrix

    /*
     * 获取子矩阵
     * 参数 r - 行索引数组
     * 参数 j0 - 列起始索引
     * 参数 j1 - 列结束索引
     * 返回值 Matrix - 子矩阵
     */
    public func getMatrix(r: Array<Int64>, j0: Int64, j1: Int64): Matrix

    /*
     * 设置单一元素， 索引对应的元素不存在时抛 IndexOutOfBoundsException
     * 参数 i - 行索引
     * 参数 j - 列起始
     * 参数 s - 元素值
     */
    public func set(i: Int64, j: Int64, s: Float64): Unit

    /*
     * 设置子矩阵，行列索引大于等于当前矩阵的行列值抛 IndexOutOfBoundsException
     * 参数 i0 - 行起始索引
     * 参数 i1 - 行结束索引
     * 参数 j0 - 列起始索引
     * 参数 j1 - 列结束索引
     * 参数 X - 子矩阵
     */
    public func setMatrix(i0: Int64, i1: Int64, j0: Int64, j1: Int64, X: Matrix): Unit

    /*
     * 设置子矩阵，行列索引大于等于当前矩阵的行列值抛 IndexOutOfBoundsException
     * 参数 r - 行索引数组
     * 参数 c - 列索引数组
     * 参数 X - 子矩阵
     */
    public func setMatrix(r: Array<Int64>, c: Array<Int64>, X: Matrix): Unit

    /*
     * 设置子矩阵，行列索引大于等于当前矩阵的行列值抛 IndexOutOfBoundsException
     * 参数 r - 行索引数组
     * 参数 j0 - 列起始索引
     * 参数 j1 - 列结束索引
     * 参数 X - 子矩阵
     */
    public func setMatrix(r: Array<Int64>, j0: Int64, j1: Int64, X: Matrix): Unit

    /*
     * 设置子矩阵，行列索引大于等于当前矩阵的行列值抛 IndexOutOfBoundsException
     * 参数 i0 - 行起始索引
     * 参数 i1 - 行结束索引
     * 参数 c - 列索引数组
     * 参数 X - 子矩阵
     */
    public func setMatrix(i0: Int64, i1: Int64, c: Array<Int64>, X: Matrix): Unit

    /*
     * 矩阵转置
     * 返回值 Matrix - 运算后矩阵
     */
    public func transpose(): Matrix

    /*
     * 矩阵L1范数计算
     * 返回值 Float64 - 运算值
     */
    public func norm1(): Float64

    /*
     * 矩阵L2范数计算
     * 返回值 Float64 - 运算值
     */
    public func norm2(): Float64 

    /*
     * 矩阵L∞范数计算
     * 返回值 Float64 - 运算值
     */
    public func normInf(): Float64

    /*
     * 矩阵F范数计算
     * 返回值 Float64 - 运算值
     */
    public func normF(): Float64

    /*
     * 矩阵取反。将矩阵的每个元素取相反数
     * 返回值 Matrix - 运算后矩阵
     */
    public func uminus(): Matrix

    /*
     * 矩阵加法运算，不符合矩阵加法规则抛 IllegalArgumentException
     * 参数 B - 要相加的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func plus(B: Matrix): Matrix

    /*
     * 矩阵加法运算，不符合矩阵加法规则抛 IllegalArgumentException
     * 参数 B - 要相加的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func plusEquals(B: Matrix): Matrix

    /*
     * 矩阵减法运算，不符合矩阵减法规则抛 IllegalArgumentException
     * 参数 B - 要相减的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func minus(B: Matrix): Matrix

    /*
     * 矩阵减法运算，不符合矩阵减法规则抛 IllegalArgumentException
     * 参数 B - 要相减的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func minusEquals(B: Matrix): Matrix

    /*
     * 矩阵逐元素乘法运算，不符合矩阵乘法规则抛 IllegalArgumentException
     * 参数 B - 要相乘的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func arrayTimes(B: Matrix): Matrix

    /*
     * 矩阵逐元素乘法运算，不符合矩阵乘法规则抛 IllegalArgumentException
     * 参数 B - 要相乘的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func arrayTimesEquals(B: Matrix): Matrix

    /*
     * 矩阵逐元素逐元素右除法运算，不符合矩阵右除法规则抛 IllegalArgumentException
     * 参数 B - 要相除的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func arrayRightDivide(B: Matrix): Matrix

    /*
     * 矩阵逐元素逐元素右除法运算，不符合矩阵右除法规则抛 IllegalArgumentException
     * 参数 B - 要相除的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func arrayRightDivideEquals(B: Matrix): Matrix

    /*
     * 矩阵逐元素逐元素左除法运算，不符合矩阵左除法规则抛 IllegalArgumentException
     * 参数 B - 要相除的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func arrayLeftDivide(B: Matrix): Matrix

    /*
     * 矩阵逐元素逐元素左除法运算，不符合矩阵左除法规则抛 IllegalArgumentException
     * 参数 B - 要相除的矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func arrayLeftDivideEquals(B: Matrix): Matrix

    /*
     * 矩阵与标量相乘，不符合矩阵乘法规则抛 IllegalArgumentException
     * 参数 B - 标量
     * 返回值 Matrix - 运算后矩阵
     */
    public func times(s: Float64): Matrix

    /*
     * 矩阵与标量相乘，不符合矩阵乘法规则抛 IllegalArgumentException
     * 参数 B - 标量
     * 返回值 Matrix - 运算后矩阵
     */
    public func timesEquals(s: Float64): Matrix

    /*
     * 线性代数矩阵乘法，不符合矩阵乘法规则抛 IllegalArgumentException
     * 参数 B - 矩阵
     * 返回值 Matrix - 运算后矩阵
     */
    public func times(B: Matrix): Matrix

    /*
     * 矩阵LU分解
     * 返回值 LUDecomposition - LU分解后的LUDecomposition
     */
    public func lu(): LUDecomposition

    /*
     * 矩阵QR分解
     * 返回值 QRDecomposition - QR分解后的QRDecomposition
     */
    public func qr(): QRDecomposition

    /*
     * 矩阵乔里斯基分解
     * 返回值 CholeskyDecomposition - 乔里斯基分解后的CholeskyDecomposition
     */
    public func chol(): CholeskyDecomposition

    /*
     * 矩阵奇异值分解
     * 返回值 SingularValueDecomposition - 奇异值分解后的SingularValueDecomposition
     */
    public func svd(): SingularValueDecomposition

    /*
     * 矩阵特征值分解
     * 返回值 EigenvalueDecomposition - 特征值分解后的EigenvalueDecomposition
     */
    public func eig(): EigenvalueDecomposition

    /*
     * 求解 A*X = B ，A,B矩阵不符合矩阵计算规则抛 IllegalArgumentException
     * 参数 B - 矩阵
     * 返回值 Matrix - X 矩阵
     */
    public func solve(B: Matrix): Matrix

    /*
     * 求解 X*A = B，也就是 A'*X' = B'，A,B矩阵不符合矩阵计算规则抛 IllegalArgumentException
     * 参数 B - 矩阵
     * 返回值 Matrix - X 矩阵
     */
    public func solveTranspose(B: Matrix): Matrix

    /*
     * 矩阵逆或伪逆
     * 返回值 Matrix - 逆矩阵
     */
    public func inverse(): Matrix

    /*
     * 矩阵行列式
     * 返回值 Float64 - 矩阵行列式的值
     */
    public func det(): Float64

    /*
     * 矩阵的秩
     * 返回值 Int64 - 矩阵的秩
     */
    public func rank(): Int64

    /*
     * 矩阵条件数
     * 返回值 Float64 - 矩阵条件数
     */
    public func cond(): Float64

    /*
     * 矩阵的迹
     * 返回值 Float64 - 矩阵的迹
     */
    public func trace(): Float64

    /*
     * 用随机元素生成矩阵
     * 参数 m - 矩阵行数
     * 参数 n - 矩阵列数
     * 返回值 Matrix - m * n 的随机数矩阵
     */
    public static func random(m: Int64, n: Int64): Matrix

    /*
     * 生成单位矩阵
     * 参数 m - 矩阵行数
     * 参数 n - 矩阵列数
     * 返回值 Matrix - m * n 的单位矩阵
     */
    public static func identity(m: Int64, n: Int64): Matrix
}

public class Maths {
    /*
     * 计算 sqrt(a^2 + b^2)
     * 参数 a - 数字a
     * 参数 b - 数字a
     * 返回值 Float64 - 运算结果
     */
    public static func hypot(a: Float64, b: Float64): Float64
}

public class CholeskyDecomposition {
    /*
     * 构造 CholeskyDecomposition 实例
     * 参数 Arg - 矩阵
     */
    public init(Arg: Matrix)

    /*
     * 矩阵是否对称且正定
     * 返回值 Bool - 判断结果
     */
    public func isSPD(): Bool

    /*
     * 获取下三角矩阵
     * 返回值 Matrix - 下三角矩阵
     */
    public func getL(): Matrix

    /*
     * 求解 A*X = B，A 为当前矩阵， B 为入参矩阵， A 要为对称正定矩阵，否则抛 Matrix4cjException 异常
     * 参数 B - 矩阵
     * 返回值 Matrix - X 矩阵
     */
    public func solve(B: Matrix): Matrix 
}

public class EigenvalueDecomposition {
    /*
     * 初始化矩阵
     * 参数 Arg - 矩阵
     */
    public init(Arg: Matrix)

    /*
     * 返回特征向量矩阵
     * 返回值 Matrix - 矩阵
     */
    public func getV(): Matrix

    /*
     * 返回特征值的实部
     * 返回值 Array<Float64>  - 实部值
     */
    public func getRealEigenvalues(): Array<Float64> 

    /*
     * 返回特征值的虚部
     * 返回值 Array<Float64>  - 虚部值
     */
    public func getImagEigenvalues(): Array<Float64>

    /*
     * 返回对角线特征值矩阵 D
     * 返回值 Matrix  - 矩阵
     */
    public func getD(): Matrix
}

public class LUDecomposition {
    /*
     * 初始化矩阵
     * 参数 Arg - 矩阵
     */
    public init(A: Matrix)

    /*
     * 判断是否为非奇异矩阵
     * 返回值 Bool  - 判断结果
     */
    public func isNonsingular(): Bool

    /*
     * 获取下三角矩阵
     * 返回值 Matrix  - 矩阵
     */
    public func getL(): Matrix

    /*
     * 获取上三角矩阵
     * 返回值 Matrix  - 矩阵
     */
    public func getU(): Matrix

    /*
     * 获取 Int64 类型的主元序列
     * 返回值 Array<Int64>  - 向量值
     */
    public func getPivot(): Array<Int64>

    /*
     * 获取 Float64 类型的主元序列
     * 返回值 Array<Float64>  - 向量值
     */
    public func getDoublePivot(): Array<Float64>

    /*
     * 返回该矩阵的行列式的值
     * 返回值 Float64  - 该矩阵的行列式的值
     */
    public func det(): Float64

    /*
     * 求解 A*X = B，A 为当前矩阵， B 为入参矩阵, A 不是非奇异矩阵抛 Matrix4cjException
     * 参数 B - 矩阵
     * 返回值 Matrix - X 矩阵
     */
    public func solve(B: Matrix): Matrix
}

public class QRDecomposition {
    /*
     * 初始化矩阵
     * 参数 Arg - 矩阵
     */
    public init(A: Matrix) {

    /*
     * 判断一个矩阵是否是满秩
     * 返回值 Bool  - 判断结果
     */
    public func isFullRank(): Bool

    /*
     * 获取 Hessenberg 矩阵
     * 返回值 Matrix  - 矩阵
     */
    public func getH(): Matrix

    /*
     * 获取上三角矩阵
     * 返回值 Matrix  - 矩阵
     */
    public func getR(): Matrix

    /*
     * 返回正交矩阵
     * 返回值 Matrix  - 矩阵
     */
    public func getQ(): Matrix

    /*
     * 求解 A*X = B，A 为当前矩阵， B 为入参矩阵， A 不是满秩矩阵抛 Matrix4cjException
     * 参数 B - 矩阵
     * 返回值 Matrix - X 矩阵
     */
    public func solve(B: Matrix): Matrix
}

public class SingularValueDecomposition {
    /*
     * 初始化矩阵
     * 参数 Arg - 矩阵
     */
    public init(Arg: Matrix)

    /*
     * 返回左奇异矢量
     * 返回值 Matrix  - 矩阵
     */
    public func getU(): Matrix 

    /*
     * 返回右奇异矢量
     * 返回值 Matrix  - 矩阵
     */
    public func getV(): Matrix

    /*
     * 返回奇异值的一维数组
     * 返回值 Array<Float64> - 数组
     */
    public func getSingularValues(): Array<Float64>

    /*
     * 返回奇异值的对角矩阵
     * 返回值 Matrix - 矩阵
     */
    public func getDiagonalValues(): Matrix

    /*
     * 矩阵L2范数计算
     * 返回值 Float64 - 运算值
     */
    public func norm2(): Float64

    /*
     * 矩阵的矩阵条件数
     * 返回值 Float64 - 运算后值
     */
    public func cond(): Float64

    /*
     * 矩阵的秩
     * 返回值 Int64 - 运算后值
     */
    public func rank(): Int64
}
```
