<div align="center">
<h1>matrix4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.2-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.59.6-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-90.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

是一个基本线性代数包，用于构造和操作真实密集矩阵。

### 特性

- 🚀 构造和操作真实密集矩阵
- 💪 支持QR分解，LU分解，乔里斯基分解，特征值分解，奇异值分解

## 软件架构

### 源码目录

```shell
.
├── doc
├── src
└── test
│   ├── DOC
│   ├── HLT
│   └── LLT
├── CHANGELOG.md
├── LICENSE.txt
├── cjpm.toml
├── README.md
└── README.OpenSource
```

- `doc`  文档目录，用于存API接口文档
- `src`  是库源码目录
- `test` 存放 HLT 测试用例、LLT 自测用例、FUZZ 测试用例和文档示例用例

### 接口说明

主要类和函数接口说明详见 [API](./doc/feature_api.md)


## 使用说明

### 编译（win/linux）

```shell
cjpm build
```

### 功能示例
#### 构造和操作真实密集矩阵

示例代码如下：

```cangjie
import matrix4cj.*

main(): Int64 {
    let tester = Test_ReadMe01()
    let test = tester.asTestSuite().runTests()
    test.failedCount + test.errorCount
}

@Test
public class Test_ReadMe01 {
    @TestCase
    func Matrix_rank(): Unit {
        var A: Matrix
        var array: Array<Array<Float64>>
        array = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 10.0]]
        A = Matrix(array)
        var B = A.rank()
        @Assert(B, 3)
    }
}
```

执行结果如下：

```shell
[ PASSED ] CASE: Matrix_rank
```

## 约束与限制

在下述版本验证通过：

    Cangjie Version: 0.59.6

## 开源协议

本项目基于 [Apache License 2.0](./LICENSE) ，请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。