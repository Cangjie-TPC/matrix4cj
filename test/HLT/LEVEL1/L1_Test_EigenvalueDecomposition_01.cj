// 3rd_party_lib:matrix4cj/target/matrix4cj
// 3rd_party_lib_ohos:matrix4cj/target/aarch64-linux-ohos/matrix4cj

import matrix4cj.*
import std.math.*

@Test
public class TestEigenvalueDecomposition {
    @TestCase
    func L1_Test_EigenvalueDecomposition_getD_01(): Unit {
        var A_Matrix = Matrix(
            [
                [9.0, 3.0, 1.0, 5.0],
                [3.0, 7.0, 5.0, 1.0],
                [1.0, 5.0, 9.0, 2.0],
                [5.0, 1.0, 2.0, 6.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_D = A_Matrix_EVD.getD()
        var expected_matrix = Matrix(
            [
                [1.113995450709542, 0.0, 0.0, 0.0],
                [0.0, 3.9295769300595853, 0.0, 0.0],
                [0.0, 0.0, 9.554080933224954, 0.0],
                [0.0, 0.0, 0.0, 16.40234668600591]
            ]
        )
        @Assert(A_Matrix_EVD_D.getRowDimension(),expected_matrix.getRowDimension())
        @Assert(A_Matrix_EVD_D.getColumnDimension(),expected_matrix.getColumnDimension())
        var rows = A_Matrix_EVD_D.getRowDimension()
        var cols = A_Matrix_EVD_D.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_matrix.get(i,j)-A_Matrix_EVD_D.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getD_02(): Unit {
        var A_Matrix = Matrix(
            [
                [2.0, 5.0, 8.0, 7.0],
                [5.0, 2.0, 2.0, 8.0],
                [7.0, 5.0, 6.0, 6.0],
                [5.0, 4.0, 4.0, 8.0]
            ]
        )

        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_D = A_Matrix_EVD.getD()
        var expected_matrix = Matrix(
            [
                [21.163540310902683, 0.0, 0.0, 0.0],
                [0.0, -4.306006764582809, 0.0, 0.0],
                [0.0, 0.0, -0.9956510147796833, 0.0],
                [0.0, 0.0, 0.0, 2.1381174684598228]
            ]
        )
        @Assert(A_Matrix_EVD_D.getRowDimension(),expected_matrix.getRowDimension())
        @Assert(A_Matrix_EVD_D.getColumnDimension(),expected_matrix.getColumnDimension())
        var rows = A_Matrix_EVD_D.getRowDimension()
        var cols = A_Matrix_EVD_D.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_matrix.get(i,j)-A_Matrix_EVD_D.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getD_03(): Unit {
        var A_Matrix = Matrix(
            [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ]
        )

        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_D = A_Matrix_EVD.getD()
        @Assert(A_Matrix_EVD_D.getRowDimension(),A_Matrix.getRowDimension())
        @Assert(A_Matrix_EVD_D.getColumnDimension(),A_Matrix.getColumnDimension())
        var rows = A_Matrix_EVD_D.getRowDimension()
        var cols = A_Matrix_EVD_D.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(A_Matrix.get(i,j)-A_Matrix_EVD_D.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getD_04(): Unit {
        var A_Matrix = Matrix(
            [
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0]
            ]
        )

        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_D = A_Matrix_EVD.getD()
        @Assert(A_Matrix_EVD_D.getRowDimension(),A_Matrix.getRowDimension())
        @Assert(A_Matrix_EVD_D.getColumnDimension(),A_Matrix.getColumnDimension())
        var rows = A_Matrix_EVD_D.getRowDimension()
        var cols = A_Matrix_EVD_D.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(A_Matrix.get(i,j)-A_Matrix_EVD_D.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getImagEigenvalues_01(): Unit {
        var A_Matrix = Matrix(
            [
                [9.0, 3.0, 1.0, 5.0],
                [3.0, 7.0, 5.0, 1.0],
                [1.0, 5.0, 9.0, 2.0],
                [5.0, 1.0, 2.0, 6.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Image = A_Matrix_EVD.getImagEigenvalues()
        var Expected_A_Matrix_EVD_Image:Array<Float64> = [0.0, 0.0, 0.0, 0.0]
        @Assert(Expected_A_Matrix_EVD_Image,A_Matrix_EVD_Image)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getImagEigenvalues_02(): Unit {
        var A_Matrix = Matrix(
            [
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Image = A_Matrix_EVD.getImagEigenvalues()
        var Expected_A_Matrix_EVD_Image: Array<Float64> = [0.0, 0.0, 0.0, 0.0]
        @Assert(Expected_A_Matrix_EVD_Image,A_Matrix_EVD_Image)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getImagEigenvalues_03(): Unit {
        var A_Matrix = Matrix(
            [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Image = A_Matrix_EVD.getImagEigenvalues()
        var Expected_A_Matrix_EVD_Image: Array<Float64> = [0.0, 0.0, 0.0, 0.0]
        @Assert(Expected_A_Matrix_EVD_Image,A_Matrix_EVD_Image)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getImagEigenvalues_04(): Unit {
        var A_Matrix = Matrix(
            [
                [2.0, 5.0, 8.0, 7.0],
                [5.0, 2.0, 2.0, 8.0],
                [7.0, 5.0, 6.0, 6.0],
                [5.0, 4.0, 4.0, 8.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Image = A_Matrix_EVD.getImagEigenvalues()
        var Expected_A_Matrix_EVD_Image: Array<Float64> = [0.0, 0.0, 0.0, 0.0]
        @Assert(Expected_A_Matrix_EVD_Image,A_Matrix_EVD_Image)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getRealEigenvalues_01(): Unit {
        var A_Matrix = Matrix(
            [
                [9.0, 3.0, 1.0, 5.0],
                [3.0, 7.0, 5.0, 1.0],
                [1.0, 5.0, 9.0, 2.0],
                [5.0, 1.0, 2.0, 6.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Real = A_Matrix_EVD.getRealEigenvalues()
        var Expected_A_Matrix_EVD_Real: Array<Float64> = [1.113995450709542, 3.9295769300595853, 9.554080933224954,
            16.40234668600591]
        @Assert(A_Matrix_EVD_Real,Expected_A_Matrix_EVD_Real)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getRealEigenvalues_02(): Unit {
        var A_Matrix = Matrix(
            [
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Real = A_Matrix_EVD.getRealEigenvalues()
        var Expected_A_Matrix_EVD_Real: Array<Float64> = [0.0, 0.0, 0.0, 0.0]
        @Assert(A_Matrix_EVD_Real,Expected_A_Matrix_EVD_Real)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getRealEigenvalues_03(): Unit {
        var A_Matrix = Matrix(
            [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Real = A_Matrix_EVD.getRealEigenvalues()
        var Expected_A_Matrix_EVD_Real: Array<Float64> = [1.0, 1.0, 1.0, 1.0]
        @Assert(A_Matrix_EVD_Real,Expected_A_Matrix_EVD_Real)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getRealEigenvalues_04(): Unit {
        var A_Matrix = Matrix(
            [
                [2.0, 5.0, 8.0, 7.0],
                [5.0, 2.0, 2.0, 8.0],
                [7.0, 5.0, 6.0, 6.0],
                [5.0, 4.0, 4.0, 8.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_Real = A_Matrix_EVD.getRealEigenvalues()
        var Expected_A_Matrix_EVD_Real: Array<Float64> = [21.163540310902683, -4.306006764582809, -0.9956510147796833,
            2.1381174684598228]
        @Assert(A_Matrix_EVD_Real,Expected_A_Matrix_EVD_Real)
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getV_01(): Unit {
        var A_Matrix = Matrix(
            [
                [9.0, 3.0, 1.0, 5.0],
                [3.0, 7.0, 5.0, 1.0],
                [1.0, 5.0, 9.0, 2.0],
                [5.0, 1.0, 2.0, 6.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_V = A_Matrix_EVD.getV()
        var expected_matrix = Matrix(
            [
                [0.5065267227928384, -0.2923303386921016, 0.593707917357719, 0.552706577709789],
                [-0.49848051937301996, -0.6198218673974198, -0.3442714578168298, 0.4988137807180691],
                [0.39880927767005075, 0.43995115789142836, -0.610995050085272, 0.5235257276252802],
                [-0.5795679519664999, 0.5803505875297517, 0.39455433208967466, 0.4142717268128481]
            ]
        )
        @Assert(A_Matrix_EVD_V.getRowDimension(),expected_matrix.getRowDimension())
        @Assert(A_Matrix_EVD_V.getColumnDimension(),expected_matrix.getColumnDimension())
        var rows = A_Matrix_EVD_V.getRowDimension()
        var cols = A_Matrix_EVD_V.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_matrix.get(i,j)-A_Matrix_EVD_V.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getV_02(): Unit {
        var A_Matrix = Matrix(
            [
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_V = A_Matrix_EVD.getV()
        var expected_matrix = Matrix(
            [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ]
        )
        @Assert(A_Matrix_EVD_V.getRowDimension(),expected_matrix.getRowDimension())
        @Assert(A_Matrix_EVD_V.getColumnDimension(),expected_matrix.getColumnDimension())
        var rows = A_Matrix_EVD_V.getRowDimension()
        var cols = A_Matrix_EVD_V.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_matrix.get(i,j)-A_Matrix_EVD_V.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getV_03(): Unit {
        var A_Matrix = Matrix(
            [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_V = A_Matrix_EVD.getV()
        @Assert(A_Matrix_EVD_V.getRowDimension(),A_Matrix.getRowDimension())
        @Assert(A_Matrix_EVD_V.getColumnDimension(),A_Matrix.getColumnDimension())
        var rows = A_Matrix_EVD_V.getRowDimension()
        var cols = A_Matrix_EVD_V.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(A_Matrix.get(i,j)-A_Matrix_EVD_V.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L1_Test_EigenvalueDecomposition_getV_04(): Unit {
        var A_Matrix = Matrix(
            [
                [2.0, 5.0, 8.0, 7.0],
                [5.0, 2.0, 2.0, 8.0],
                [7.0, 5.0, 6.0, 6.0],
                [5.0, 4.0, 4.0, 8.0]
            ]
        )
        var A_Matrix_EVD = EigenvalueDecomposition(A_Matrix)
        var A_Matrix_EVD_V = A_Matrix_EVD.getV()
        var expected_matrix = Matrix(
            [
                [0.5221402566778668, 0.8402454749753158, -0.1453084980671888, -0.49605125729440236],
                [0.40138660797330256, -0.4447338590298808, 0.9802352411509356, 0.7758424353831935],
                [0.5684794484810822, -0.2964453254374863, -0.40515519633448227, -0.9938739984678833],
                [0.4930410327251712, -0.10048025006502033, -0.17494872648396198, 0.5718952095633261]
            ]
        )
        @Assert(A_Matrix_EVD_V.getRowDimension(),expected_matrix.getRowDimension())
        @Assert(A_Matrix_EVD_V.getColumnDimension(),expected_matrix.getColumnDimension())
        var rows = A_Matrix_EVD_V.getRowDimension()
        var cols = A_Matrix_EVD_V.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_matrix.get(i,j)-A_Matrix_EVD_V.get(i,j))<1e-6,true)
            }
        }
    }
}
