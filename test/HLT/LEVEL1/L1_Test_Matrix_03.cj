// 3rd_party_lib:matrix4cj/target/matrix4cj
// 3rd_party_lib_ohos:matrix4cj/target/aarch64-linux-ohos/matrix4cj

import matrix4cj.*
import std.math.*

@Test
public class TestMatrix {
    var data_matrix: Array<Array<Float64>> = [
        [1.80, 2.88, 2.05, -0.89],
        [5.25, -2.95, -0.95, -3.80],
        [1.58, -2.69, -2.90, -1.04],
        [-1.11, -0.66, -0.59, 0.80]
    ]

    @TestCase
    func L0_Test_Matrix_getMatrix01_01(): Unit {
        var matrix01 = Matrix(data_matrix)
        var result = matrix01.getMatrix(1, 2, 2, 2)
        var expected_result = Matrix([[-0.95], [-2.90]])
        @Assert(result.getRowDimension(),expected_result.getRowDimension())
        @Assert(result.getColumnDimension(),expected_result.getColumnDimension())
        var rows = result.getRowDimension()
        var cols = result.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_result.get(i,j)-result.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L0_Test_Matrix_getMatrix02_01(): Unit {
        var matrix01 = Matrix(data_matrix)
        var result = matrix01.getMatrix([1, 2], [2, 2])
        var expected_result = Matrix([[-0.95, -0.95], [-2.90, -2.90]])

        @Assert(result.getRowDimension(),expected_result.getRowDimension())
        @Assert(result.getColumnDimension(),expected_result.getColumnDimension())
        var rows = result.getRowDimension()
        var cols = result.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_result.get(i,j)-result.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L0_Test_Matrix_getMatrix03_01(): Unit {
        var matrix01 = Matrix(data_matrix)
        var result = matrix01.getMatrix([1, 2, 3], 2, 2)
        var expected_result = Matrix(
            [
                [-0.95],
                [-2.90],
                [-0.59]
            ]
        )

        @Assert(result.getRowDimension(),expected_result.getRowDimension())
        @Assert(result.getColumnDimension(),expected_result.getColumnDimension())
        var rows = result.getRowDimension()
        var cols = result.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_result.get(i,j)-result.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L0_Test_Matrix_getMatrix04_01(): Unit {
        var matrix01 = Matrix(data_matrix)
        var result = matrix01.getMatrix(1, 1, [1, 2, 3])
        var expected_result = Matrix([[-2.95, -0.95, -3.80]])

        @Assert(result.getRowDimension(),expected_result.getRowDimension())
        @Assert(result.getColumnDimension(),expected_result.getColumnDimension())
        var rows = result.getRowDimension()
        var cols = result.getColumnDimension()
        for (i in 0..rows) {
            for (j in 0..cols) {
                @Assert(abs(expected_result.get(i,j)-result.get(i,j))<1e-6,true)
            }
        }
    }
    @TestCase
    func L0_Test_Matrix_getRowDimension_01(): Unit {
        var matrix01 = Matrix(data_matrix)
        @Assert(matrix01.getRowDimension(),4)
    }
    @TestCase
    func L0_Test_Matrix_getRowDimension_02(): Unit {
        var matrix01 = Matrix([[1.0]])
        @Assert(matrix01.getRowDimension(),1)
    }
    @TestCase
    func L0_Test_Matrix_getRowPackedCopy_01(): Unit {
        var matrix01 = Matrix(data_matrix)
        var result = matrix01.getRowPackedCopy()
        var expected_result: Array<Float64> = [1.80, 2.88, 2.05, -0.89, 5.25, -2.95, -0.95, -3.80, 1.58, -2.69, -2.90,
            -1.04, -1.11, -0.66, -0.59, 0.80]

        for (i in 0..result.size) {
            @Assert(abs(expected_result[i]-result[i])<1e-6,true)
        }
    }
}
