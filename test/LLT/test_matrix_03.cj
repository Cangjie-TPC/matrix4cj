// EXEC: cjc %import-path %L %l %f
// EXEC: ./main

import std.unittest.*
import std.unittest.testmacro.*
import matrix4cj.*

main(): Int64 {
    let tester = MatrixTester03()
    let test = tester.asTestSuite().runTests()
    test.failedCount + test.errorCount
}

@Test
public class MatrixTester03 {
    @TestCase
    func Matrix_getMatrix_int_int_int_A01(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 10.0]]
        A = Matrix(array)
        B = A.getMatrix(0, 1, [0, 1])
        @Assert(B.getRowDimension(),2)
        @Assert(B.getColumnDimension(),2)
        @Assert(A.getRowDimension(),3)
        @Assert(A.getColumnDimension(),3)
    }

    @TestCase
    func Matrix_getMatrix_int_int_int_A02(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[27.0, 82.0, 210.0], [4.0, 5.0, 6.0]]
        A = Matrix(array)
        B = A.getMatrix(0, 1, [0, 1])
        @Assert(B.getRowDimension(),2)
        @Assert(B.getColumnDimension(),2)
        @Assert(A.getRowDimension(),2)
        @Assert(A.getColumnDimension(),3)
    }

    @TestCase
    func Matrix_getMatrix_int_int_int_A03(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[1.0, 32.0, 13.0], [4.0, 52.0, 16.0], [73.0, 83.0, 120.0]]
        A = Matrix(array)
        B = A.getMatrix(1, 1, [1, 1])
        @Assert(B.getRowDimension(),1)
        @Assert(B.getColumnDimension(),2)
        @Assert(B.getArray(),[[52.0,52.0]])
    }

    @TestCase
    func Matrix_getMatrix_int_int_int_A04(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[11.0, 6.0, 13.0], [89.0, 83.0, 3.0]]
        A = Matrix(array)
        B = A.getMatrix(1, 1, [1, 0])
        @Assert(B.getRowDimension(),1)
        @Assert(B.getColumnDimension(),2)
        @Assert(B.getArray(),[[83.0,89.0]])
    }

    @TestCase
    func Matrix_getMatrix_int_A_int_int01(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 10.0]]
        A = Matrix(array)
        B = A.getMatrix([0, 1], 0, 1)
        @Assert(B.getRowDimension(),2)
        @Assert(B.getColumnDimension(),2)
        @Assert(A.getRowDimension(),3)
        @Assert(A.getColumnDimension(),3)
    }

    @TestCase
    func Matrix_getMatrix_int_A_int_int02(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[4.0, 54.0, 89.0], [34.0, 33.0, 300.0]]
        A = Matrix(array)
        B = A.getMatrix([0, 1], 0, 1)
        @Assert(B.getRowDimension(),2)
        @Assert(B.getColumnDimension(),2)
        @Assert(A.getRowDimension(),2)
        @Assert(A.getColumnDimension(),3)
    }

    @TestCase
    func Matrix_getMatrix_int_A_int_int03(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[11.0, 12.0, 33.0], [41.0, 33.0, 6.0], [70.0, 18.0, 120.0]]
        A = Matrix(array)
        B = A.getMatrix([0, 0], 0, 1)
        @Assert(B.getRowDimension(),2)
        @Assert(B.getColumnDimension(),2)
        @Assert(B.getArray(),[[11.0,12.0], [11.0,12.0]])
    }

    @TestCase
    func Matrix_getMatrix_int_A_int_int04(): Unit {
        var A: Matrix
        var B: Matrix
        var array: Array<Array<Float64>>
        array = [[8.0, 80.0, 100.0], [9.0, 8.0, 36.0]]
        A = Matrix(array)
        B = A.getMatrix([1, 1], 1, 1)
        @Assert(B.getRowDimension(),2)
        @Assert(B.getColumnDimension(),1)
        @Assert(B.getArray(),[[8.0], [8.0]])
    }

    @TestCase
    func Matrix_set01(): Unit {
        var A: Matrix
        var array: Array<Array<Float64>>
        array = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 10.0]]
        A = Matrix(array)
        A.set(0, 1, 66.0)
        @Assert(A.getArray()[0][1],66.0)
    }

    @TestCase
    func Matrix_set02(): Unit {
        var A: Matrix
        var array: Array<Array<Float64>>
        array = [[46.0, 65.0, 66.0], [62.0, 82.0, 140.0]]
        A = Matrix(array)
        try {
            A.set(0, 3, 33.0)
        } catch (e: Exception) {
        }
        @Assert(A.getArray()[1][1],82.0)
    }

    @TestCase
    func Matrix_setMatrix_int_int_int_int_Jama_Matrix01(): Unit {
        var A: Matrix
        var array: Array<Array<Float64>>
        var array2: Array<Array<Float64>>
        array = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 10.0]]
        array2 = [[10.0, 20.0, 30.0], [40.0, 50.0, 60.0], [70.0, 80.0, 100.0]]
        A = Matrix(array)
        var C = Matrix(array2);
        A.setMatrix(0,1,0,1,C)
        @Assert(A.getArray()[0][1],array2[0][1])
    }

    @TestCase
    func Matrix_setMatrix01(): Unit {
        var A: Matrix
        var array: Array<Array<Float64>>
        var array2: Array<Array<Float64>>
        array = [[54.0, 15.0, 116.0], [71.0, 18.0, 410.0]]
        array2 = [[310.0, 220.0, 130.0], [270.0, 180.0, 90.0]]
        A = Matrix(array)
        var B = Matrix(array2);
        A.setMatrix(0,1,0,0,B)
        @Assert(A.getArray()[0][1],15.0)
    }
}
